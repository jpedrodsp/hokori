import socket
import shell

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(("127.0.0.1", 64010))
sock.listen()
conn, addr = sock.accept()
data = ""
halt = False
with conn:
    print("connected")
    while True and not halt:
        buffersize = 1024
        buffer = conn.recv(buffersize)
        if buffer == b"quit":
            halt = True
            break
        data = bytes.decode(buffer, "utf-8")
        if data:
            s = data.split()
            if s[0] == "exec":
                shell.execute(' '.join(s[1:]))
            if s[0] == "transfer":
                

sock.close()