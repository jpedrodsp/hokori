import subprocess

def execute(command):
    print("Executing: ", command)
    try:
        result = subprocess.run(command, capture_output=True)
        print("Command executed succesfully!")
        return result.stdout.decode("utf-8")
    except Exception:
        print("Command executed with errors!")
        return None
    pass